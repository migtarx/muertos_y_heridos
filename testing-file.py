from random import randrange
class player:
    def __init__(self, name, trys, time):
        self.__name = name
        self.__trys = trys
        self.__time = time

    def getName(self):
        return self.__name
    def getTrys(self):
        return self.__trys
    def getTime(self):
        return self.__time


player1 = player("Juan", 5, 5.55)
player2 = player("Alex", 2, 9.62)
player3 = player("Miguel", 3, 6)
customObjects = [player1, player2, player3]
customObjects.sort(key=lambda x: x.getTrys(), reverse=False)

for player in customObjects:
    print("Sorted Date from obj: " + str(player.getName()) + " with title: " + str(player.getTrys()) + " intentos " + str(player.getTime()))
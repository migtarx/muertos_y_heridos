import termtables as tt
import os
from random import randrange
from art import *
from timerModule import Timer

#General variables
debugMode = True
firstStartUp = True

#Runtime temporal data
playerName = ""
alamacenDeIntentos = []
players = []

class intento:
    def __init__(self, tryNum, number, deads, woundeds):
        self.__tryNum = tryNum
        self.__number = number
        self.__deads = deads
        self.__woundeds = woundeds

    def getTryNum(self):
        return self.__tryNum
    def getNumber(self):
        numberStringBuild = str(self.__number[0]) + str(self.__number[1]) + str(self.__number[2]) + str(self.__number[3])
        numberStringBuild = int(numberStringBuild)
        return numberStringBuild
    def getDeads(self):
        return self.__deads
    def getWoundeds(self):
        return self.__woundeds

class player:
    def __init__(self, name, trys, time):
        self.__name = name
        self.__trys = trys
        self.__time = time

    def getName(self):
        return self.__name
    def getTrys(self):
        return self.__trys
    def getTime(self):
        return self.__time

def cls():
    os.system("cls")

def printResults():
    datos = []
    for intento in alamacenDeIntentos:
        tupla = [intento.getTryNum(), intento.getNumber(), intento.getDeads(), intento.getWoundeds()]
        datos.append(tupla)
    table = tt.to_string(
        datos,
        header=["Intentos", "Número", "Muertos", "Heridos"],
        style=tt.styles.ascii_thin_double,
    )
    return table

def savePlayerProgress(playerName, playerTrys, playerTime):
    playerGame = player(playerName, playerTrys, playerTime)
    players.append(playerGame)

def numeroAleatorio():
    numeroFinal = []
    i = 0
    while i != 4:
        numero = randrange(10)
        if numero not in numeroFinal:
            numeroFinal.append(numero)
            i += 1
    return numeroFinal

def cifrasRepetidas(numeroUsuario):
    comprobacion = []
    for cifra in numeroUsuario:
        if cifra in comprobacion:
            return True
        else:
            comprobacion.append(cifra)
    return False
def showRanking():
    tableData = []
    for player in players:
        tuple = [player.getName(), player.getTrys(), player.getTime()]
        tableData .append(tuple)
    tableData.sort(key=lambda x: x.getTrys(), reverse=False)
    table = tt.to_string(
        tableData,
        header=["Nombre", "Intentos", "Tiempo"],
        style=tt.styles.ascii_thin_double,
    )
    print(table)

def contructorNumeroJugador(inputNumeroJugador):
    numeroUsuario = []
    if len(inputNumeroJugador) != 4:
        return False
    for cifra in inputNumeroJugador:
        try:
            numero = int(cifra)
            numeroUsuario.append(numero)
        except ValueError:
            print("Tiene que ser un valor numérico")
            return False
    if cifrasRepetidas(numeroUsuario):
        return False
    else:
        return numeroUsuario

def checkDeadsWoundeds(numCallback, numerogen):
    numDeads = 0
    numWoundeds = 0
    for i in range(0,4):
        if numCallback[i] == numerogen[i]:
            numDeads+=1
    for num in numCallback:
        if num in numerogen:
            numWoundeds+=1
    numWoundeds = numWoundeds - numDeads
    deadsWoundeds = [numDeads, numWoundeds]
    return deadsWoundeds
def play():
    cls()
    t = Timer()
    alamacenDeIntentos.clear()
    numerogen = numeroAleatorio()
    if debugMode:
        print(numerogen)
    playerName= input("Introduce tu nombre: ")
    t.start()
    print("Acabamos de generar un número para que juegues. Comenzemos!")
    numIntento = 0
    while True:
        print("Piensa un número. Sus cifras no se pueden repetir. Puede comenzar por cero")
        numero = input("Introduce tu número: ")
        callback = contructorNumeroJugador(numero)
        if callback == False:
            print("El número que has introducido no cumple los requisitos")
        else:
            numIntento += 1
            print("Tenemos tu número!")
            deadsWoundeds = checkDeadsWoundeds(callback, numerogen)
            intentoVar = intento(numIntento, callback, deadsWoundeds[0], deadsWoundeds[1])
            alamacenDeIntentos.append(intentoVar)
            print(printResults())
            if deadsWoundeds[0] == 4:
                playedTime = round(t.stop(), 2)
                print("ENHORABUENA!!!! HAS ACERTADO EL NÚMERO en " + str(numIntento) + " intentos en " + str(playedTime) + " segundos")
                while True:
                    guardar = input("¿Quiéres guardar tu progreso? (S/N): ")
                    if guardar.lower() == "s":
                        print(playerName)
                        savePlayerProgress(playerName, numIntento, playedTime)
                        return
                    elif guardar.lower() == "n":
                        return
                    else:
                        print("Solo puedes elegir entre \"S\" y \"N\"")
            if numIntento == 14:
                playedTime = round(t.stop(), 2)
                print("Has llegado al limite de intentos. Tiempo de juego: " + str(playedTime) + " segundos")
                while True:
                    guardar = input("¿Quiéres guardar tu progreso? (S/N): ")
                    if guardar.lower() == "s":
                        savePlayerProgress(playerName, numIntento, playedTime)
                    elif guardar.lower() == "n":
                        return
                    else:
                        print("Solo puedes elegir entre \"S\" y \"N\"")

# MENU PRINCIPAL
tprint('Muertos  y  heridos')
print("           Bienvenido al juego de Muertos y Heridos de Miguel \"migtarx\" Puerta")

while True:
    if firstStartUp:
        firstStartUp = False
    else:
        print()
        #cls()
    print("Menú de selección")
    print("[1] --> Jugar")
    print("[2] --> Ranking")
    print("[3] --> Salir")
    eleccion = input("Selecciona una de las 3 opciones: ")
    try:
        eleccion = int(eleccion)
        if eleccion == 1:
            play()
        elif eleccion == 2:
            showRanking()
        elif eleccion == 3:
            print("Saliendo...")
            exit()
        else:
            print("Opción no válida")
    except ValueError:
        print("Tiene que ser un valor numérico")
        continue
